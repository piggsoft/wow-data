package com.piggsoft.wow.data;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.piggsoft.wow.data.mapper")
public class WowDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(WowDataApplication.class, args);
    }

}
