package com.piggsoft.wow.data.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName MARKET_ITEM_DETAIL
 */
@TableName(value ="MARKET_ITEM_DETAIL")
@Data
public class MarketItemDetail implements Serializable {
    /**
     * 
     */
    @TableId
    private Integer itemId;

    /**
     * 
     */
    private String itemName;

    /**
     * 
     */
    private Object minPrice;

    /**
     * 
     */
    private Object maxPrice;

    /**
     * 
     */
    private Object avgPrice;

    /**
     * 
     */
    private Object count;

    /**
     * 
     */
    private Date time;

    /**
     * 
     */
    private Object 95Price;

    /**
     * 
     */
    private Object 90Price;

    /**
     * 
     */
    private Object 80Price;

    /**
     * 
     */
    private Object 85Price;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        MarketItemDetail other = (MarketItemDetail) that;
        return (this.getItemId() == null ? other.getItemId() == null : this.getItemId().equals(other.getItemId()))
            && (this.getItemName() == null ? other.getItemName() == null : this.getItemName().equals(other.getItemName()))
            && (this.getMinPrice() == null ? other.getMinPrice() == null : this.getMinPrice().equals(other.getMinPrice()))
            && (this.getMaxPrice() == null ? other.getMaxPrice() == null : this.getMaxPrice().equals(other.getMaxPrice()))
            && (this.getAvgPrice() == null ? other.getAvgPrice() == null : this.getAvgPrice().equals(other.getAvgPrice()))
            && (this.getCount() == null ? other.getCount() == null : this.getCount().equals(other.getCount()))
            && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()))
            && (this.get95Price() == null ? other.get95Price() == null : this.get95Price().equals(other.get95Price()))
            && (this.get90Price() == null ? other.get90Price() == null : this.get90Price().equals(other.get90Price()))
            && (this.get80Price() == null ? other.get80Price() == null : this.get80Price().equals(other.get80Price()))
            && (this.get85Price() == null ? other.get85Price() == null : this.get85Price().equals(other.get85Price()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getItemId() == null) ? 0 : getItemId().hashCode());
        result = prime * result + ((getItemName() == null) ? 0 : getItemName().hashCode());
        result = prime * result + ((getMinPrice() == null) ? 0 : getMinPrice().hashCode());
        result = prime * result + ((getMaxPrice() == null) ? 0 : getMaxPrice().hashCode());
        result = prime * result + ((getAvgPrice() == null) ? 0 : getAvgPrice().hashCode());
        result = prime * result + ((getCount() == null) ? 0 : getCount().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        result = prime * result + ((get95Price() == null) ? 0 : get95Price().hashCode());
        result = prime * result + ((get90Price() == null) ? 0 : get90Price().hashCode());
        result = prime * result + ((get80Price() == null) ? 0 : get80Price().hashCode());
        result = prime * result + ((get85Price() == null) ? 0 : get85Price().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", itemId=").append(itemId);
        sb.append(", itemName=").append(itemName);
        sb.append(", minPrice=").append(minPrice);
        sb.append(", maxPrice=").append(maxPrice);
        sb.append(", avgPrice=").append(avgPrice);
        sb.append(", count=").append(count);
        sb.append(", time=").append(time);
        sb.append(", 95Price=").append(95Price);
        sb.append(", 90Price=").append(90Price);
        sb.append(", 80Price=").append(80Price);
        sb.append(", 85Price=").append(85Price);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}