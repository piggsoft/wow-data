package com.piggsoft.wow.data.service;

import com.piggsoft.wow.data.entity.SysConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface SysConfigService extends IService<SysConfig> {

}
