package com.piggsoft.wow.data.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggsoft.wow.data.entity.SysConfig;
import com.piggsoft.wow.data.service.SysConfigService;
import com.piggsoft.wow.data.mapper.SysConfigMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig>
        implements SysConfigService {

    public String getValue(String key, String type) {
        QueryWrapper<SysConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(!StringUtils.isEmpty(key), SysConfig::getSysKey, key)
                .eq(!StringUtils.isEmpty(type), SysConfig::getSysType, type);
        SysConfig one = getOne(queryWrapper);
        return one.getSysValue();
    }

    public String getTsmService() {
        return null;
    }

}




