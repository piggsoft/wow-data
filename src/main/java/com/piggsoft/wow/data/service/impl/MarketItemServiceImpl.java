package com.piggsoft.wow.data.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggsoft.wow.data.entity.MarketItem;
import com.piggsoft.wow.data.service.MarketItemService;
import com.piggsoft.wow.data.mapper.MarketItemMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class MarketItemServiceImpl extends ServiceImpl<MarketItemMapper, MarketItem>
implements MarketItemService{

}




