package com.piggsoft.wow.data.service;

import com.piggsoft.wow.data.entity.MarketItemDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface MarketItemDetailService extends IService<MarketItemDetail> {

}
