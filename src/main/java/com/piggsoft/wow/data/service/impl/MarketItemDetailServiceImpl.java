package com.piggsoft.wow.data.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.piggsoft.wow.data.entity.MarketItemDetail;
import com.piggsoft.wow.data.service.MarketItemDetailService;
import com.piggsoft.wow.data.mapper.MarketItemDetailMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class MarketItemDetailServiceImpl extends ServiceImpl<MarketItemDetailMapper, MarketItemDetail>
implements MarketItemDetailService{

}




