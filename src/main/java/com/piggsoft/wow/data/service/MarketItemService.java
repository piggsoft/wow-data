package com.piggsoft.wow.data.service;

import com.piggsoft.wow.data.entity.MarketItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface MarketItemService extends IService<MarketItem> {

}
