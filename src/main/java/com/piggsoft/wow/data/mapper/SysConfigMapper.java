package com.piggsoft.wow.data.mapper;

import com.piggsoft.wow.data.entity.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.piggsoft.wow.data.entity.SysConfig
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}




