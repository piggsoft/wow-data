package com.piggsoft.wow.data.mapper;

import com.piggsoft.wow.data.entity.MarketItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.piggsoft.wow.data.entity.MarketItem
 */
public interface MarketItemMapper extends BaseMapper<MarketItem> {

}




