package com.piggsoft.wow.data.mapper;

import com.piggsoft.wow.data.entity.MarketItemDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.piggsoft.wow.data.entity.MarketItemDetail
 */
public interface MarketItemDetailMapper extends BaseMapper<MarketItemDetail> {

}




