-- auto-generated definition
create table SYS_CONFIG
(
    ID        INT auto_increment,
    SYS_KEY   VARCHAR(50),
    SYS_TYPE  CHAR(4),
    SYS_VALUE TEXT,
    constraint SYS_CONFIG_PK
        primary key (ID)
);